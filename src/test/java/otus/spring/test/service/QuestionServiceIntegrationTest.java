package otus.spring.test.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class QuestionServiceIntegrationTest {
    @Autowired
    QuestionService questionService;

    @Test
    void testPrintQuestions() throws Exception {
        String text = tapSystemOut(() -> questionService.printQuestions());
        assertEquals("""
                How are you doing?
                """, text);
    }
}
