package otus.spring.test.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class QuestionServiceUnitTest {
    private final ResourceLoader resourceLoader = mock(ResourceLoader.class);
    private final IOService ioService = Mockito.mock(IOService.class);

    private final QuestionService questionService = new QuestionService(resourceLoader, ioService);

    @Test
    @DisplayName("score should be 1/1")
    void testPrintQuestions() throws IOException {
        when(ioService.readLine()).thenReturn("Test", "User", "2");

        String mockFile = "How are you doing?, \"Well,Fine\", 1";
        InputStream is = new ByteArrayInputStream(mockFile.getBytes());
        Resource resource = mock(Resource.class);
        when(resourceLoader.getResource(any())).thenReturn(resource);
        when(resource.getInputStream()).thenReturn(is);

        questionService.runQuiz();

        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(ioService, times(4)).println(argumentCaptor.capture());

        String capturedArgument = argumentCaptor.getValue();
        String expected = "Test User, your score is 1/1";
        assertEquals(expected, capturedArgument);
    }
}
