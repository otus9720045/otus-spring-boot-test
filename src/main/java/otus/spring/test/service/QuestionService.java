package otus.spring.test.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import otus.spring.test.model.Question;
import otus.spring.test.model.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class QuestionService {
    @Value("${app.questions-path}")
    private String questionsPath;
    private final ResourceLoader resourceLoader;
    private final IOService ioService;

    public QuestionService(ResourceLoader resourceLoader, IOService ioService) {
        this.resourceLoader = resourceLoader;
        this.ioService = ioService;
    }

    public void printQuestions() {
        readQuestions()
                .stream()
                .map(Question::question)
                .forEach(ioService::println);
    }

    public void runQuiz() {
        var user = askName();
        List<Question> questions = readQuestions();
        Map<Integer, Integer> questionToAnswer = getAnswers(questions);
        Map<Integer, Integer> questionToUserAnswer = askQuestions(questions);
        int countOfRightAnswers = getCountOfRightAnswers(questionToUserAnswer, questionToAnswer);
        printResult(countOfRightAnswers, questions.size(), user);
    }

    private User askName() {
        ioService.print("Please enter your first name: ");
        String firstName = ioService.readLine();
        while (firstName.isBlank()) {
            ioService.print("First name can't be empty: ");
            firstName = ioService.readLine();
        }

        ioService.print("Please enter your last name: ");
        String lastName = ioService.readLine();
        while (lastName.isBlank()) {
            ioService.print("Last name can't be empty: ");
            lastName = ioService.readLine();
        }

        return new User(firstName, lastName);
    }

    private Map<Integer, Integer> askQuestions(List<Question> questions) {
        Map<Integer, Integer> questionToUserAnswer = new HashMap<>();

        IntStream.range(0, questions.size())
                .forEach(i -> {
                    var question = questions.get(i);
                    ioService.println(question.question());

                    var answers = question.answers();
                    IntStream.range(0, answers.size())
                            .forEach(index ->
                                    ioService.println((index + 1) + ": " + answers.get(index))
                            );

                    ioService.print("Your answer: ");
                    int answer = Integer.parseInt(ioService.readLine());
                    questionToUserAnswer.put(i, answer - 1);
                });

        return questionToUserAnswer;
    }

    private List<Question> readQuestions() {
        Resource resource = resourceLoader.getResource(questionsPath);
        return readQuestions(resource);
    }

    private List<Question> readQuestions(Resource resource) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8))) {
            List<String> lines = reader.lines()
                    .filter(line -> !line.isBlank())
                    .toList();
            return IntStream.range(0, lines.size())
                    .mapToObj(i -> {
                        List<String> values = Arrays.asList(lines.get(i).split(", "));
                        return new Question(
                                i,
                                values.get(0),
                                Arrays.asList(values.get(1).replaceAll("\"", "").split(",")),
                                Integer.parseInt(values.get(2))
                        );
                    })
                    .toList();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private Map<Integer, Integer> getAnswers(List<Question> questions) {
        return questions.stream()
                .collect(Collectors.toMap(Question::id, Question::correctAnswerIndex));
    }

    private int getCountOfRightAnswers(Map<Integer, Integer> questionToUserAnswer, Map<Integer, Integer> questionToAnswer) {
        return questionToUserAnswer.entrySet()
                .stream()
                .filter(entry -> questionToAnswer.get(entry.getKey()).equals(entry.getValue()))
                .toList()
                .size();
    }

    private void printResult(int countOfRightAnswers, int totalCountOfAnswers, User user) {
        String text = String.format("%s %s, your score is %d/%d", user.firstName(), user.lastName(), countOfRightAnswers, totalCountOfAnswers);
        ioService.println(text);
    }
}
