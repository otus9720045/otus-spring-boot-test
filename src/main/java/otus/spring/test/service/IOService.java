package otus.spring.test.service;

import org.springframework.stereotype.Service;

import java.util.Scanner;

@Service
public class IOService {
    private final Scanner scanner = new Scanner(System.in);

    public void print(String text) {
        System.out.print(text);
    }

    public void println(String text) {
        System.out.println(text);
    }

    public String readLine() {
        return scanner.nextLine();
    }

    public void println(Object o) {
        System.out.println(o);
    }
}
