package otus.spring.test.model;

import java.util.List;

public record Question(int id, String question, List<String> answers, int correctAnswerIndex) {
}
