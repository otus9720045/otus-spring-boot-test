package otus.spring.test.model;

public record User(String firstName, String lastName) {
}
