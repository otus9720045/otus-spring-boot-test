package otus.spring.test.runner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import otus.spring.test.service.QuestionService;

@Component
public class QuizRunner implements CommandLineRunner {
    private final QuestionService questionService;

    public QuizRunner(QuestionService questionService) {
        this.questionService = questionService;
    }

    @Override
    public void run(String... args) {
        questionService.runQuiz();
    }
}
